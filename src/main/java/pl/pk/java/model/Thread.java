package pl.pk.java.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Getter
@NoArgsConstructor
@javax.persistence.Entity
@Table(name = "POSTS")
public class Thread extends Entity {

  @ManyToOne(fetch = FetchType.EAGER, optional = false)
  @JoinColumn(name = "user_id", nullable = false)
  @OnDelete(action = OnDeleteAction.CASCADE)
  private User user;

  private String title;

  private String content;

  private Date date;

  public Thread(User user, String content, String title, Date date) {
    this.user = user;
    this.content = content;
    this.title = title;
    this.date = date;
  }
}
