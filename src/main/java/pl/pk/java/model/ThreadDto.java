package pl.pk.java.model;

import lombok.Getter;
import lombok.NoArgsConstructor;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

@NoArgsConstructor
@Getter
public class ThreadDto {
  private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

  private Long id;

  private String title;

  private String content;

  private String date;

  private User user;

  public ThreadDto(Long id, String title, String content, User user) {
    this.id = id;
    this.title = title;
    this.user = user;
    this.content = content;
  }

  public Date getSubmissionDateConverted(String timezone) throws ParseException {
    dateFormat.setTimeZone(TimeZone.getTimeZone(timezone));
    return dateFormat.parse(this.date);
  }

  public void setSubmissionDate(Date date, String timezone) {
    dateFormat.setTimeZone(TimeZone.getTimeZone(timezone));
    this.date = dateFormat.format(date);
  }
}
