package pl.pk.java.commands;

import pl.pk.java.model.Post;

public interface PostCommand {
  void execute(Post post);
}
