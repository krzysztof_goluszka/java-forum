package pl.pk.java.commands;

import pl.pk.java.model.Post;

public class LikeCommand implements PostCommand {
  private int amount;

  public LikeCommand(int amount) {
    this.amount = amount;
  }

  @Override
  public void execute(Post post) {
    post.upVoteBy(amount);
  }
}