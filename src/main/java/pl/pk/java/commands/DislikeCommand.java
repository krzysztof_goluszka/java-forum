package pl.pk.java.commands;

import pl.pk.java.model.Post;

public class DislikeCommand implements PostCommand {
  private int amount;

  public DislikeCommand(int amount) {
    this.amount = amount;
  }

  @Override
  public void execute(Post post) {
    post.downVoteBy(amount);
  }
}
