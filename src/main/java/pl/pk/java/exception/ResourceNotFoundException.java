package pl.pk.java.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@ResponseStatus(value = NOT_FOUND, reason = "Could not find resource")
public class ResourceNotFoundException extends RuntimeException {}
