package pl.pk.java.storage;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.pk.java.model.Post;

import java.util.List;

public interface PostStorage extends JpaRepository<Post, Long> {

  List<Post> findByUserId(Long userId);

  List<Post> findByPostId(Long postId);
}
