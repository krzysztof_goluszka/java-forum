package pl.pk.java.storage;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.pk.java.model.Thread;

import java.util.List;

@Repository
public interface ThreadStorage extends JpaRepository<Thread, Long> {
  List<Thread> findByUserId(Long userId);
}
