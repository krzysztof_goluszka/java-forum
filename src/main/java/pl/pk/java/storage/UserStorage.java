package pl.pk.java.storage;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.pk.java.model.User;

@Repository
public interface UserStorage extends JpaRepository<User, Long> {}
