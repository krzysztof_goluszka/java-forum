package pl.pk.java.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pl.pk.java.commands.DislikeCommand;
import pl.pk.java.commands.LikeCommand;
import pl.pk.java.model.Post;
import pl.pk.java.service.PostService;

import javax.validation.Valid;

@RestController
public class PostController {

  private PostService postService;

  public PostController(PostService postService) {
    this.postService = postService;
  }

  @PutMapping("/comments/{commentId/upvote")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public Post upvote(@PathVariable Long commentId, @Valid @RequestBody int amount) {

    return postService.vote(commentId, new LikeCommand(amount));
  }

  @PutMapping("/comments/{commentId/downvote")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public Post downvote(@PathVariable Long commentId, @Valid @RequestBody int amount) {

    return postService.vote(commentId, new DislikeCommand(amount));
  }
}
