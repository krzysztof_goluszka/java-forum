package pl.pk.java.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pl.pk.java.controller.request.UserRequestParam;
import pl.pk.java.model.User;
import pl.pk.java.service.UserService;

import javax.validation.Valid;
import java.util.List;

@RestController
public class UsersController {

  private UserService userService;

  public UsersController(UserService userService) {
    this.userService = userService;
  }

  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public List<User> getAllUsers() {
    return userService.getAllUsers();
  }

  @GetMapping("/users/{id}")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public User getUser(@PathVariable long id) {
    return userService.getUser(id);
  }

  @PostMapping("/users")
  @ResponseStatus(HttpStatus.CREATED)
  @ResponseBody
  public User createUser(@Valid @RequestBody UserRequestParam newUserParam) {
    return userService.createUser(newUserParam.toUser());
  }
}
