package pl.pk.java.controller.request;

import lombok.NoArgsConstructor;
import pl.pk.java.model.User;

import javax.validation.constraints.NotBlank;

@NoArgsConstructor
public class UserRequestParam {
  @NotBlank(message = "can't be empty")
  private String firstName;

  @NotBlank(message = "can't be empty")
  private String lastName;

  private UserRequestParam(String firstName, String lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
  }

  public static Builder newBuilder() {
    return new Builder();
  }

  public User toUser() {
    return new User(firstName, lastName);
  }

  @NoArgsConstructor
  public static class Builder {
    private String firstName;
    private String lastName;

    public Builder firstName(String firstName) {
      this.firstName = firstName;
      return this;
    }

    public Builder lastName(String lastName) {
      this.lastName = lastName;
      return this;
    }

    public UserRequestParam build() {
      return new UserRequestParam(this.firstName, this.lastName);
    }
  }
}
