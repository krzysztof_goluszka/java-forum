package pl.pk.java.controller.request;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.pk.java.model.Thread;
import pl.pk.java.model.User;

import javax.validation.constraints.NotBlank;
import java.time.ZonedDateTime;
import java.util.Date;

@Getter
@JsonRootName("post")
@NoArgsConstructor
public class ThreadRequestParam {
  @NotBlank(message = "can't be empty")
  private String title;

  @NotBlank(message = "can't be empty")
  private String content;

  private Date date;

  private ThreadRequestParam(String title, String content) {
    this.title = title;
    this.content = content;
    this.date = Date.from(ZonedDateTime.now().toInstant());
  }

  public static Builder newBuilder() {
    return new Builder();
  }

  public Thread toPost(User user) {
    return new Thread(user, title, content, date);
  }

  @NoArgsConstructor
  public static class Builder {
    private String title;
    private String content;

    public Builder title(String title) {
      this.title = title;
      return this;
    }

    public Builder content(String content) {
      this.content = content;
      return this;
    }

    public ThreadRequestParam build() {
      return new ThreadRequestParam(this.title, this.content);
    }
  }
}
