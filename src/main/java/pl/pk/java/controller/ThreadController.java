package pl.pk.java.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pl.pk.java.controller.request.PostRequestParam;
import pl.pk.java.controller.request.ThreadRequestParam;
import pl.pk.java.model.Post;
import pl.pk.java.model.Thread;
import pl.pk.java.model.ThreadDto;
import pl.pk.java.service.PostService;
import pl.pk.java.service.ThreadService;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class ThreadController {

  private ThreadService threadService;
  private PostService postService;

  public ThreadController(ThreadService threadService, PostService postService) {
    this.threadService = threadService;
    this.postService = postService;
  }

  @GetMapping("/posts")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public List<ThreadDto> getAll() {
    List<Thread> threads = threadService.getAll();
    return threads.stream().map(this::convertToDto).collect(Collectors.toList());
  }

  @GetMapping("/posts/{id}")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public ThreadDto get(@PathVariable Long id) {
    Thread thread = threadService.getById(id);
    return convertToDto(thread);
  }

  @PostMapping("/users/{userId}/posts")
  @ResponseStatus(HttpStatus.CREATED)
  @ResponseBody
  public ThreadDto create(
      @PathVariable Long userId, @Valid @RequestBody ThreadRequestParam newPostParam) {
    Thread thread = threadService.create(userId, newPostParam);
    return convertToDto(thread);
  }

  @GetMapping("/users/{userId}/posts")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public List<ThreadDto> getByUserId(@PathVariable Long userId) {
    List<Thread> threads = threadService.getByUserId(userId);
    return threads.stream().map(this::convertToDto).collect(Collectors.toList());
  }

  @GetMapping("/posts/{postId}/comments")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public List<Post> getCommentsByPostId(@PathVariable Long postId) {
    return postService.getByPostId(postId);
  }

  @PostMapping("/posts/{postId}/comments")
  @ResponseStatus(HttpStatus.CREATED)
  @ResponseBody
  public Post create(
      @PathVariable Long postId, @Valid @RequestBody PostRequestParam postRequestParam) {

    return postService.create(postId, postRequestParam);
  }

  private ThreadDto convertToDto(Thread thread) {
    ThreadDto threadDto = new ThreadDto(thread.getId(), thread.getTitle(), thread.getContent(), thread.getUser());

    threadDto.setSubmissionDate(thread.getDate(), "UTC");

    return threadDto;
  }
}
