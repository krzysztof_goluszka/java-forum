package pl.pk.java.service;

import org.springframework.stereotype.Service;
import pl.pk.java.controller.request.ThreadRequestParam;
import pl.pk.java.exception.ResourceNotFoundException;
import pl.pk.java.model.Thread;
import pl.pk.java.storage.ThreadStorage;
import pl.pk.java.storage.UserStorage;

import java.util.List;
import java.util.Optional;

@Service
public class ThreadServiceImplementation implements ThreadService {

  private final ThreadStorage threadStorage;

  private final UserStorage userStorage;

  public ThreadServiceImplementation(ThreadStorage threadStorage, UserStorage userStorage) {
    this.threadStorage = threadStorage;
    this.userStorage = userStorage;
  }

  @Override
  public List<Thread> getAll() {
    return threadStorage.findAll();
  }

  @Override
  public Thread getById(Long id) {
    Optional<Thread> post = threadStorage.findById(id);

    if (!post.isPresent()) {
      throw new ResourceNotFoundException();
    }

    return post.get();
  }

  @Override
  public Thread create(Long userId, ThreadRequestParam threadRequestParam) {
    return userStorage
        .findById(userId)
        .map(user -> threadStorage.save(threadRequestParam.toPost(user)))
        .orElseThrow(ResourceNotFoundException::new);
  }

  @Override
  public List<Thread> getByUserId(Long userId) {
    return threadStorage.findByUserId(userId);
  }
}
