package pl.pk.java.service;

import pl.pk.java.model.User;

import java.util.List;

public interface UserService {
  List<User> getAllUsers();

  User getUser(Long id);

  User createUser(User user);
}
