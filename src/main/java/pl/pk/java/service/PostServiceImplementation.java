package pl.pk.java.service;

import org.springframework.stereotype.Service;
import pl.pk.java.commands.PostCommand;
import pl.pk.java.controller.request.PostRequestParam;
import pl.pk.java.exception.ResourceNotFoundException;
import pl.pk.java.model.Post;
import pl.pk.java.model.Thread;
import pl.pk.java.model.User;
import pl.pk.java.storage.PostStorage;
import pl.pk.java.storage.ThreadStorage;
import pl.pk.java.storage.UserStorage;

import java.util.List;
import java.util.Optional;

@Service
public class PostServiceImplementation implements PostService {

  private PostStorage postStorage;

  private ThreadStorage threadStorage;

  private UserStorage userStorage;

  public PostServiceImplementation(
      ThreadStorage threadStorage,
      UserStorage userStorage,
      PostStorage postStorage) {
    this.threadStorage = threadStorage;
    this.userStorage = userStorage;
    this.postStorage = postStorage;
  }

  @Override
  public Post create(Long postId, PostRequestParam postRequestParam) {
    Long userId = postRequestParam.getUserId();

    Optional<User> user = userStorage.findById(userId);

    if (!user.isPresent()) {
      throw new ResourceNotFoundException();
    }

    Optional<Thread> post = threadStorage.findById(postId);

    if (!post.isPresent()) {
      throw new ResourceNotFoundException();
    }

    Post comment = new Post(post.get(), user.get(), postRequestParam.getContent());

    return postStorage.save(comment);
  }

  @Override
  public List<Post> getByPostId(Long postId) {
    return postStorage.findByPostId(postId);
  }

  @Override
  public Post vote(Long commentId, PostCommand command) {

    Optional<Post> maybeComment = postStorage.findById(commentId);

    if (!maybeComment.isPresent()) {
      throw new ResourceNotFoundException();
    }

    Post post = maybeComment.get();
    command.execute(post);

    return postStorage.save(post);
  }
}
