package pl.pk.java.service;

import pl.pk.java.controller.request.ThreadRequestParam;
import pl.pk.java.model.Thread;

import java.util.List;

public interface ThreadService {
  Thread getById(Long id);

  List<Thread> getByUserId(Long userId);

  List<Thread> getAll();

  Thread create(Long userId, ThreadRequestParam threadRequestParam);
}
