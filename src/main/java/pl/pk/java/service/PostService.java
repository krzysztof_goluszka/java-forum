package pl.pk.java.service;

import pl.pk.java.commands.PostCommand;
import pl.pk.java.controller.request.PostRequestParam;
import pl.pk.java.model.Post;

import java.util.List;

public interface PostService {
  Post create(Long postId, PostRequestParam postRequestParam);

  List<Post> getByPostId(Long postId);

  Post vote(Long commentId, PostCommand command);
}
