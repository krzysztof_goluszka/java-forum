package pl.pk.java.service;

import org.springframework.stereotype.Service;
import pl.pk.java.exception.ResourceNotFoundException;
import pl.pk.java.model.User;
import pl.pk.java.storage.UserStorage;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

  private final UserStorage userStorage;

  public UserServiceImpl(UserStorage userStorage) {
    this.userStorage = userStorage;
  }

  @Override
  public List<User> getAllUsers() {
    return userStorage.findAll();
  }

  @Override
  public User getUser(Long id) {
    Optional<User> user = userStorage.findById(id);

    if (!user.isPresent()) {
      throw new ResourceNotFoundException();
    }

    return user.get();
  }

  @Override
  public User createUser(User newUser) {
    return userStorage.save(newUser);
  }
}
